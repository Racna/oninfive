<?php require 'lessc.compile.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 9 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gte IE 9]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
  <head>

    <meta charset="UTF-8" />

    <title>HTML5 blank site</title>
    
    <meta name="author" content="Felix Laukel" />
    <meta name="description" content="This is a brand new website. Nicely written in HTML5." />
    <meta name="keywords" content="hipster, buzzwords, go, here" />

    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui" />

    <link type="text/css" rel="stylesheet" href="css/style.css" />
    
    <!-- use client-side less.js compiler:
    <link type="text/less" rel="stylesheet" href="less/style.less">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.4.0/less.min.js"></script>
    -->
    
    <link rel="shortcut icon" href="images/favicon.png" />
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/lib/oi5/utils.js"></script>
    <script type="text/javascript" src="js/lib/oi5/widgets.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

  </head>

  <body class="main custom layout">

    <section class="section">
      <div class="content">
        
      </div>
    </section>



    <?php include('templates.tpl'); ?>

  </body>

</html>
