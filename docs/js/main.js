(function ($, window) {

  oi5.utils.useComments();

  $(document).ready(function(e) {
    
    $('a[href^=#]').on('click', function(e) {
      e.preventDefault();

      if ($(this).closest('.tabs').length > 0) return;

      var id = $(this).attr('href');
      var offset = -$('#header').height() - 24;
      oi5.utils.scrollTo(id, 800, offset);
    });

  });



})(jQuery, this);