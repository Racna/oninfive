<?php
define('SRC_PATH', '../html/');
require SRC_PATH.'lessc.compile.php';
?>
<!DOCTYPE html>
<!--[if lt IE 9 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gte IE 9]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
  <head>

    <meta charset="UTF-8" />

    <title>OnInFive feature demo &amp; documentation</title>
    
    <meta name="author" content="Felix Laukel" />
    <meta name="description" content="This is a brand new website. Nicely written in HTML5." />
    <meta name="keywords" content="hipster, buzzwords, go, here" />

    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui" />

    <link type="text/css" rel="stylesheet" href="css/style.css" />
    
    <!-- use client-side less.js compiler:
    <link type="text/less" rel="stylesheet" href="less/style.less">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.4.0/less.min.js"></script>
    -->
    
    <link rel="shortcut icon" href="images/favicon.png" />
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="../html/js/lib/oi5/utils.js"></script>
    <script type="text/javascript" src="../html/js/lib/oi5/widgets.js"></script>
    <script type="text/javascript" src="../html/js/lib/oi5/objects.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

  </head>

  <body class="main demo layout">

    <div class="content-loader overlay"  hidden>
      <div class="vertical-center">
        <p>
          <span class="h4">This is loading ...</span><br />
          <small>( add fancy animation here )</small>
        </p>
      </div>
    </div>

    <!--<div class="off-screen-container right">-->

      <header id="header" class="section sticky small-screen">
        <div class="content group">

          <a id="logo" href="/" hidden>
            <img src="http://placehold.it/120x60" alt="Logo" />
          </a>

          <a class="toggle-nav icon-bars icon-only fl-right" data-toggle-nav="main-nav" href="#"></a>
          
          <nav id="main-nav" class="toggleable overlay">

            <a class="toggle-nav icon-cross icon-only fl-right small-screen-only" data-toggle-nav="main-nav" href="#"></a>

            <ul class="responsive navi">
              <li><span class="label title-font">OnInFive</span></li>
              <li class="active"><a href="#">Home</a></li>
              <li>
                <a href="#">Submenu</a>
                <ul class="vertical">
                  <li><a href="#">Lorem ipsum</a></li>
                  <li>
                    <a href="#">Submenu</a>
                    <ul class="vertical">
                      <li><a href="#">Lorem ipsum</a></li>
                      <li><a href="#">Dolor sit amet</a></li>
                      <li><a href="#">Vestibulum</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Dolor sit amet</a></li>
                </ul>
              </li>
              <li class="mega">
                <a href="#">Mega</a>
                <div class="dropdown">
                  <div class="responsive grid gap">
                    <div class="col size-33">
                      <h4>Hello</h4><hr />
                    </div>
                    <div class="col size-33">
                      <h4>Mega</h4><hr />
                    </div>
                    <div class="col size-33">
                      <h4>Dropdown</h4><hr />
                    </div>
                  </div>
                </div>
              </li>
              <li><a href="./">Docs</a></li>
              <li><a href="../html/">HTML</a></li>
            </ul>
          </nav>


        </div>
      </header>

      <section id="hero" class="section fs-block responsive-font-size" data-fs-amount="0.66">
        <div class="content vertical-center">

          <h1>H1 Headline<br />with second line</h1>
          <p>Percental vertical hero section | vertical-centered content</p>
          
        </div>
      </section>

      <section class="section gap">
        <div class="content">
        
          <h3>Grid system</h3>

          <h4>Responsive amount of columns per row</h4>

          <div class="responsive grid">

            <!-- REPEAT: 4x -->
            <div class="col size-25 medium-size-33 small-size-50">
              <img src="http://placehold.it/300x100" alt="" class="block paragraph" />
            </div>

          </div>

          <p>Above shows usage of multiple *-size classes in order to change width of columns depending on viewport.</p>

          <hr />
          
          <h4>Nested grids</h4>
          
          <div class="debug responsive grid gap">
            <div class="row">
              <!-- REPEAT: 3 -->
              <div class="col size-33">
                <div class="row">
                  <!-- REPEAT: 2 -->
                  <div class="col size-50">
                    <p>&nbsp;</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <!-- REPEAT: 4 -->
              <div class="col size-25">
                <div class="row">
                  <!-- REPEAT: 2 -->
                  <div class="col size-50">
                    <p>&nbsp;</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <hr />

        </div>
      </section>

      <section class="section">
        <div class="content">

          <div class="responsive grid gap">
            <div class="col size-33">
              <figure class="center figure">
                <img class="circle" src="http://placehold.it/200x200" alt="" />
                <!-- COPY: caption -->
                <figcaption>
                  <h3>Figure Headline</h3>
                  <p>
                    Lorem ipsum dolor sit amet, <a href="#">vestibulum</a> ullamcorper sapien, nam maecenas, non eu ornare congue.
                  </p>
                </figcaption>
              </figure>
            </div>
            <div class="col size-33">
              <figure class="center figure">
                <img class="circle" src="http://placehold.it/200x200" alt="" />
                <!-- PASTE: caption -->
              </figure>
            </div>
            <div class="col size-33">
              <figure class="center figure">
                <img class="circle" src="http://placehold.it/200x200" alt="" />
                <!-- PASTE: caption -->
              </figure>
            </div>
          </div>

          <hr />

          <div class="medium-responsive flip grid gap">
            <div class="col size-33">
              <div class="checklist">
                <i class="icon-mug"></i>
                <h3>Flipped Feature #1</h3>
                <p>
                  Lorem ipsum dolor sit amet, vestibulum ullamcorper sapien, nam maecenas, non eu ornare congue.
                </p>
              </div>
            </div>
            <div class="col size-33">
              <div class="checklist">
                <i class="icon-rocket"></i>
                <h3>Flipped Feature #2</h3>
                <p>
                  Lorem ipsum dolor sit amet, vestibulum ullamcorper sapien, nam maecenas, non eu ornare congue.
                </p>
              </div>
            </div>
            <div class="col size-33">
              <div class="checklist">
                <i class="icon-box"></i>
                <h3>Flipped Feature #3</h3>
                <p>
                  Lorem ipsum dolor sit amet, vestibulum ullamcorper sapien, nam maecenas, non eu ornare congue.
                </p>
              </div>
            </div>
          </div>
          
          <hr />        
          
        </div>
      </section>

      <section class="section">
        <div class="content">

          <div class="responsive grid gap">
            <div class="row">

              <article class="col size-66">

                <figure class="cine paragraph">
                  <img src="images/placeholder01.jpg" alt="" />
                  <figcaption>
                    <div class="vertical-center">
                      <h2>Hello, H2 Headline</h2>
                      <p>Where're you heading?</p>
                    </div>
                  </figcaption>
                </figure>

                <h3>H3 Headline<br />with second line</h3>
                <p class="subline">Subline Lorem ipsum dolor</p>

                <p>
                  Lorem ipsum dolor sit amet, vestibulum ullamcorper sapien, nam maecenas, non eu ornare congue ligula risus sed. Eget tempus condimentum pede suscipit, ante quam aliquam maecenas justo quam dolor.
                </p>
                <h4>H4 Headline</h4>
                <blockquote>
                    Lorem ipsum dolor sit amet, vestibulum ullamcorper sapien, nam maecenas, non eu ornare congue ligula risus sed. Eget tempus condimentum pede suscipit, ante quam aliquam maecenas justo quam dolor.
                </blockquote>
  <pre class="code"><code>(function($) {
    $(function() {
      // document ready
    });
  })(jQuery);</code></pre>

                <h4>Yes, and classic tables!</h4>

                <table>
                  <thead>
                    <tr>
                      <th>Column 1</th>
                      <th>Column 2</th>
                      <th>Column 3</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Row 1</td>
                      <td>Table content</td>
                      <td>Table content</td>
                    </tr>
                    <tr>
                      <td>Row 2</td>
                      <td>Table content</td>
                      <td>Table content</td>
                    </tr>
                    <tr>
                      <td>Row 3</td>
                      <td>Table content</td>
                      <td>Table content</td>
                    </tr>
                    <tr>
                      <td>Row 4</td>
                      <td>Table content</td>
                      <td>Table content</td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="3">Footer</td>
                    </tr>
                  </tfoot>
                </table>

              </article>

              <aside class="col size-33">
                <div class="sticky" data-offset="40">
                  <h4>Sidebar</h4>
                  <p>
                    <a class="btn" onclick="new oi5.objects.Dialog('Yes, it haz migthy Dialog!');">Dialog Button</a>
                  </p>
                  <p>
                    <a class="btn block icon-angle-right" href="#">Block Icon Button</a>
                  </p>
                  <div class="display-group paragraph">
                    <a class="btn icon-angle-right">First</a>
                    <span class="btn icon-angle-right">Second</span>
                    <a class="btn icon-angle-right">Third</a>
                  </div>
                  <form class="single-form paragraph">
                    <input placeholder="Single form">
                    <button class="btn icon-search2 icon-only">
                      <span class="visually-hidden">Submit</span>
                    </button>
                  </form>
                  <ul class="tabs navi">
                    <li><a href="#tab-1">Tab #1</a></li>
                    <li><a href="#tab-2">Tab #2</a></li>
                    <li><a href="#tab-3">Tab #3</a></li>
                  </ul>
                  <div class="tabs container">
                    <div id="tab-1" class="tab-content">Tab content #1</div>
                    <div id="tab-2" class="tab-content">Tab content #2</div>
                    <div id="tab-3" class="tab-content">Tab content #3</div>
                  </div>
                </div>
              </aside>

            </div>
          </div>

        </div>
      </section>

      <footer id="footer" class="section">
        <div class="content">
          <hr />
          <div class="responsive grid gap">
            <!-- REPEAT: 4 -->
            <div class="col size-25">
              <h5>Footer Column</h5>
              <ul class="vertical navi">
                <li><a href="" class="icon-angle-right">Lorem ipsum dolor</a></li>
                <li><a href="" class="icon-angle-right">Lorem ipsum dolor</a></li>
                <li><a href="" class="icon-angle-right">Lorem ipsum dolor</a></li>
              </ul>
            </div>

          </div>
        </div>
      </footer>

    <!--</div>-->

    <?php include(SRC_PATH.'templates.tpl'); ?>

  </body>

</html>
