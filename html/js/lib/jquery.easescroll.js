/*! jquery.easescroll.js v0.9.0 | MIT License | (c) 2014 by Felix Laukel (http://laukel.com) */
;(function($) {
   
  $.easeScroll = function(options) {
    
    var settings = $.extend({
      step: 60,
      ease: .25
    }, options || {});
    
    var $window = $(window),
        $document = $(document),
        top = $window.scrollTop(),
        y = top, // target scroll position
        ease = Math.min(.99, Math.max(.001, settings.ease)),
        step = settings.step,
        $body = (navigator.userAgent.indexOf('AppleWebKit') !== -1) ? $('body') : $('html'),
        scrolling = false;

    $document.on('mousewheel DOMMouseScroll', function(e) {
      e.preventDefault();
      var delta = e.originalEvent.wheelDelta || -e.originalEvent.detail || -e.originalEvent.deltaY;

      y += (delta < 0) ? step : -step;
      y = Math.min($document.height() - $window.innerHeight() + 10, Math.max(0, y));

      if (!scrolling) animate();

    });

    $window.on('scroll', function (e) {
      if (!scrolling) top = y = $window.scrollTop();
    });


    var animate = function() {
      if (Math.round(y) === Math.round(top)) {
        scrolling = false;
        top = y;
        return;

      } else {
        scrolling = true;
        top += (y - top) * ease;
        $body.scrollTop(top);

        requestAnimationFrame(animate);
      }

    }
  
  };

})(jQuery);
