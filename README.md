##About OnInFive

OnInFive is an HTML5 front-end framework. It is developed by Felix Laukel and used at eCentral.
There are many more frameworks out there, that to similar things as OnInFive and some of them do even more!

The main motivation for me developing OnInFive is:
*Learning and understanding the backgrounds of good front-end design.*

Yet, OnInFive can do more than just responsive grids and basic page layouts.

Fork and see for youself.

Be warned:
Docs aren't ready yet :(