/*! widgets.js v1.4.3 | MIT License | (c) 2015 by Felix Laukel (http://laukel.com) */

window.oi5 = window.oi5 || {};

(function(public, $, window) {


  public.autoStart = true;


  // toggle responsive overlay nav
  public.toggleableNav = function() {
    
    $('.toggle-nav').on('click', function(e) {
      e.preventDefault();

      var id = $(this).attr('data-toggle-nav') || $(this).attr('href');
      if (id.charAt(0) !== '#') id = '#'+id;

      var $nav = $(id);
      if (!$nav.data('toggle')) $nav.data('toggle', this);

      if ($nav.is('.active')) {
        $nav.trigger('hide');
      } else {
        $nav.trigger('show');
      }
    });

    var $navs = $('nav.toggleable');
    

    $navs.each(function() {
      // base behaviour for every nav
      $(this).on('show', function() {
        $(this).addClass('active');
        $($(this).data('toggle')).addClass('active');
      }).on('hide', function() {
        $(this).removeClass('active');
        $($(this).data('toggle')).removeClass('active');
      });
    });


    $navs.filter('.sidebar').each(function() {

      $(this).on('show', function() {
        $('body').css({ 'overflow-y': 'hidden' });
      }).on('hide', function() {
        $('body').css({ 'overflow-y': 'scroll' });
      }).on('mousedown touchstart', function(e) {
        if ($(this).is('.active')) {
          if(!$(e.target).closest('.inner-wrap').length) {
            e.preventDefault();
            e.stopPropagation();
            $(this).trigger('hide');
          }
        }
      }).wrapInner('<div class="inner-wrap"></div>');

    });

    $navs.filter('.off-screen').each(function() {

      $(this).on('show', function() {
        $(this).closest('.off-screen-container').addClass('active');
      }).on('hide', function() {
        $(this).closest('.off-screen-container').removeClass('active');
      });

    });


    $navs.filter('.dropdown').each(function() {
      // nothing special here
    });
  }



  // page loader
  public.contentLoader = function() {
    var removeLoader = function() {
      $(this).remove();
    };
    $(window).on('load', function() {
      $('.content-loader').each(function() {
        if (!Modernizr.csstransitions || !Modernizr.cssanimations) {
          removeLoader.apply(this); // use fadeOut -> removeLoader instead?
        } else {
          $(this).one('transitionend animationend', $.proxy(removeLoader, this));
        }
      });
    });
  }



  // init tab navigations
  public.tabs = function() {
    $('.tabs.navi').on('click activate', 'a[href^=#]', function(e) {
      e.preventDefault();
      var id = $(this).attr('href');
      $(this).closest('li').addClass('active').siblings().removeClass('active');
      $(id).addClass('active').siblings().removeClass('active');
    }).children('li:first-child').find('a').trigger('activate');
  }



  // auto-wrap table + cell container for vertical-centered content,
  // does only work if parent element has a height set
  public.verticalCenter = function() {
    $('.vertical-center').each(function() {
      $(this).wrap('<div class="table-wrap" />').wrap('<div class="cell-wrap" />');
    });
  }



  // automatically fill grid *DEPRECATED*
  // makes only sense in use with 'data-use-rows' for auto grouping within rows
  public.autoGrid = function() {
    $('.grid[data-col-size]').each(function() {
      var self = this;
      var size = parseInt($(self).attr('data-col-size'));
      if (!size) return;

      var $items = $(self).children().detach();
      var numItems = $items.length;
      var useRows = $(self).is('[data-use-rows]');
      var n = 0, row = 0, col = 0;
      var $row = null;
      var numCols = Math.floor(100/size);
      var numRows = (useRows) ? Math.ceil(numItems / numCols) : 1;
      while (row++ < numRows) {
        $row = $('<div class="row">').appendTo(self);
        while (col++ < numCols) {
          $row.append('<div class="col size-'+size+'">');
          if (++n >= numItems) break;
        }
        col = 0;
      }
      
      $items.each(function(index) {
        var i = (useRows) ? index : index % numCols;
        $(self).find('.col:eq('+ i +')').append(this);
      });
    });
  }



  // use image-fx styles *DEPRECATED*
  public.imageFX = function() {
    $(window).load(function() { // use window load, chrome fix

      $('img[data-image-fx]').each(function() { // deprecated
        var self = this;

        var display = $(self).css('display');
        if (display === 'inline') { display = 'inline-block'; }

        var $wrapper = $('<div class="image-fx" />').css({
          'display': display,
          'margin-top': $(self).css('margin-top'),
          'margin-right': $(self).css('margin-right'),
          'margin-bottom': $(self).css('margin-bottom'),
          'margin-left': $(self).css('margin-left')
        }).addClass($(self).attr('data-image-fx') || 'fx-scale');
        $(self).wrap($wrapper);
        
      });
    });
  }


  // enable fullscreen/percentual blocks
  public.fsBlock = function() {
    // fullscreen blocks
    $('.fs-block').on('update', function() {
      var ww = $(window).width();
      var wh = $(window).height();

      var $self = $(this);
      var bw = $self.width(); // block width
      var ratio = parseFloat($(this).attr('data-fs-ratio')) || 1.78; // 16:9
      var amount = parseFloat($(this).attr('data-fs-amount')) || 1;
      var offset = parseFloat($(this).attr('data-fs-offset')) || 0;

      var halign = .5, valign = .5;
      if (typeof($(this).attr('data-fs-halign')) === 'string')
        halign = parseFloat($(this).attr('data-fs-halign'));
      if (typeof($(this).attr('data-fs-valign')) === 'string')
        valign = parseFloat($(this).attr('data-fs-valign'));

      var invRatio = 1/ratio;
      var ch = (amount <= 4) ? (wh-offset) * amount : amount; // calc height
      if (bw / ch > ratio) {
        var w = bw;
        var h = bw*invRatio;
      } else {
        var w = ch*ratio;
        var h = ch;
      }

      var bh = Math.min(h, ch); // block height
      $self.css({ 'height': bh });

      $self.children('.fs-content').css({
        'width': w,
        'height': h,
        'left': -(w - bw)*halign,
        'top': -(h - bh)*valign
      });
    });

    $(window).resize(function(e) {
      $('.fs-block').trigger('update');
    });
  }



  // enable dynamic font-size depending on screen width
  public.responsiveFontSize = function() {
    $(window).resize(function(e) {
      var ww = $(window).width();
      var wh = $(window).height();

      // responsive font-size
      $('.responsive-font-size').each(function() {
        var baseFontSize = parseFloat($(this).parent().css('font-size')) || 16;
        var defaultWidth = parseFloat($(this).attr('data-default-width')) || 1280;
        var ratio = (ww)/defaultWidth;
        var fontSize = Math.min(baseFontSize*1.25, Math.max(baseFontSize*.75, baseFontSize * ratio)); // max, min, default
        $(this).css({ 'font-size': Math.round(fontSize) });
      });
    });
  }



  // enable sticky elements *EXPERIMENTAL*
  // safe use only with .section elements for now
  public.sticky = function() {

    $('.sticky').on('check', function() {
      var $self = $(this);

      var isSmallLayout = $('html').is('.layout-small');
      var qualified = !(isSmallLayout && $self.is(':not(.small-screen)'));

      var scrollTop = $(window).scrollTop();
      var customOffset = parseInt($self.attr('data-offset')) || 0;
      var originalOffset = $self.data('originalOffset');
      var offset = originalOffset - customOffset;
      var isFixed = $self.is('.fixed');

      if ( qualified && scrollTop >= offset ) {

        $(this).css({
          'top': customOffset,
          'width': $self.parent().width()
        }).addClass('fixed').next().css({
          'margin-top': $self.outerHeight(true) // TODO: use placeholder element?
        });

      } else if (isFixed) {

        $self.css({
          'top': '',
          'width': ''
        }).removeClass('fixed').next().css({
          'margin-top': 0
        });

      } else {
        // store offset for each sticky element
        $self.data('originalOffset', $self.offset().top);

      }
    });

    // watch for resize
    $(window).resize(function(e) {

      $('.sticky').trigger('check');

    });

    // main scroll listener
    $(window).scroll(function() {

      $('.sticky').trigger('check');
      
    });
  }



  // add watcher for elements within viewport *EXPERIMENTAL*
  // TODO: add custom event on 'update'
  public.watchViewport = function() {

    $('.watch-viewport').on('init', function() {
      var $self = $(this);

      var selector = $self.attr('data-watch');
      var $content = (!selector) ? $self.children() : $self.find(selector);
      var $nav = $( $self.attr('data-watch-nav') || null );
      var treshold = parseFloat($self.attr('data-watch-treshold')) || .33;

      $self.data('$content', $content);
      $self.data('$nav', $nav);
      $self.data('treshold', treshold);

    }).trigger('init');

    $('.watch-viewport').on('update', function() {
      var $self = $(this);

      // cache important values
      var $content = $self.data('$content');
      var $nav = $self.data('$nav');
      var treshold = $self.data('treshold');

      var scrollHeight = $(window).outerHeight();
      var scrollTop = $(window).scrollTop();
      var scrollCenter = scrollTop + scrollHeight*.5;
      var scrollBottom = scrollTop + scrollHeight;
      var scrollTreshold = scrollHeight * treshold;

      var bottom = $self.height();

      // find current page within viewport
      var id = $content.each(function() {

        var top = $(this).offset().top;
        var height = $(this).outerHeight(true);

        var inViewport = (scrollTop + scrollTreshold > top && scrollTop + scrollTreshold < top + height && scrollBottom < bottom+scrollHeight*.9);
        $(this).toggleClass('active', inViewport);
        
        var isVisible = (scrollBottom-1 > top && scrollTop+1 < top + height);
        $(this).toggleClass('visible', isVisible);

      }).filter('.active').attr('id');

      if ($nav.length > 0) {
        $nav.find('li').removeClass('active');
        $nav.find('li').toggleClass('undefined', !id);

        $nav.find('a[href=#'+id+'], [data-watch=#'+id+']').parents('li').addClass('active');
      }

    });

    $(window).scroll(function(e) {
      $('.watch-viewport').trigger('update');
    });
  }



  // init modules by list of function names
  public.init = function(mods) {
    for (var i = 0; i < mods.length; i++) {
      if (public[mods[i]] instanceof Function) public[mods[i]]();
    }
  }



  $(document).ready(function() {

    if (public.autoStart) {

      public.init([
        'toggleableNav',
        'contentLoader',
        'tabs',
        'verticalCenter',
        'autoGrid',
        'imageFX',
        'fsBlock',
        'responsiveFontSize',
        'sticky',
        'watchViewport'
      ]);

    }

    // auto trigger window resize after dom ready
    $(window).resize();   

  });


  $(window).load(function() {

    // auto trigger window resize after page load
    $(window).resize();

    // set class when page is fully loaded; use with css transitions :)
    $('html').addClass('loaded');

  });


  $(window).resize(function(e) {

    // add classes for media query break-points that have been set within less/css
    $('html').toggleClass('layout-small', oi5.utils.testCSSClass('small-screen-only'));
    $('html').toggleClass('layout-medium', oi5.utils.testCSSClass('medium-screen-only'));

  });



})(window.oi5.widgets = {}, jQuery, window);