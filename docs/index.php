<?php
define('SRC_PATH', '../html/');
define('INC_PATH', 'inc/');

require SRC_PATH.'lessc.compile.php';
?>
<!DOCTYPE html>
<!--[if lt IE 9 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gte IE 9]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
  <head>

    <meta charset="UTF-8" />

    <title>OnInFive feature demo &amp; documentation</title>
    
    <meta name="author" content="Felix Laukel" />
    <meta name="description" content="This is a brand new website. Nicely written in HTML5." />
    <meta name="keywords" content="hipster, buzzwords, go, here" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui" />

    <link type="text/css" rel="stylesheet" href="css/style.css" />
    
    <!-- use client-side less.js compiler:
    <link type="text/less" rel="stylesheet" href="less/style.less">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.4.0/less.min.js"></script>
    -->
    
    <link rel="shortcut icon" href="images/favicon.png" />
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="../html/js/lib/oi5/utils.js"></script>
    <script type="text/javascript" src="../html/js/lib/oi5/widgets.js"></script>
    <script type="text/javascript" src="../html/js/lib/oi5/objects.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

  </head>

  <body class="main custom layout">

    <section id="header" class="sticky section">
      <div class="content group">

        <nav id="head-nav">
          <a id="logo" class="push-left" href="./">OnInFive</a>
          <ul class="navi push-right">
            <li><a href="demo.php">Demo</a></li>
            <li><a href="#">Download</a></li>
            <li><a href="#">Fork me</a></li>
          </ul>
        </nav>
      </div>
    </section>

    <section id="hero">
      <div class="content">
        <h1>Documentation</h1>
        <p class="subline">What this is, what to expect and how to use it. With examples and pictures!</p>
      </div>
    </section>


    <section id="page">

      <div class="content">
        <div class="responsive grid">
          <div class="col size-20">

            <aside id="sidebar" class="sticky" data-offset="80">

              <nav id="main-nav">
                <ul class="vertical navi">
                  <li><a href="#about">About</a></li>
                  <li><a href="#getting-started">Getting started</a></li>
                  <li>
                    <a href="#basic-layout">Basic layout</a>

                    <ul class="vertical">
                      <li><a href="#grid">The Grid</a></li>
                      <li><a href="#navigation">Navigation</a></li>
                    </ul>


                  </li>

                  <li>
                    <a href="#widgets">Widgets</a>
                    <ul class="vertical">
                      <li><a href="#toggleable-nav">Toggleable Nav</a></li>
                      <li><a href="#vertical-center">Vertical Center</a></li>
                      <li><a href="#fullscreen-block">Fullscreen Block</a></li>
                      <li><a href="#">Sticky</a></li>
                      <li><a href="#">Auto Grid</a></li>
                      <li><a href="#">Watch Viewport</a></li>
                      <li><a href="#">Tabs</a></li>
                      <li><a href="#">Content Loader</a></li>
                    </ul>
                  </li>
                </ul>
              </nav>
              
            </aside>

          </div>
          <div class="col size-80">
            
            <div id="content" class="watch-viewport" data-watch=".content-block" data-watch-nav="#main-nav">

              <div id="about" class="content-block">
                <?php include INC_PATH . 'about.html' ?>
              </div>

              <div id="getting-started" class="content-block">
                <?php include INC_PATH . 'getting-started.html' ?>
              </div>

              <div id="basic-layout" class="content-block">
                <h2>Basic layout</h2>
                <?php //include INC_PATH . 'basic-layout.html' ?>
              </div>

              <div id="grid" class="content-block">
                <?php include INC_PATH . 'grid.html' ?>
              </div>

              <div id="navigation" class="content-block">
                <h2>Navigation</h2>
                <?php //include INC_PATH . 'navigation.html' ?>
              </div>

              <div id="widgets" class="content-block">
                <h2>Widgets</h2>
                <?php //include INC_PATH . 'widgets.html' ?>
              </div>

              <div id="toggleable-nav" class="content-block">
                <h2>Toggleable Nav</h2>
                <?php //include INC_PATH . 'toggleable-nav.html' ?>
              </div>

              <div id="vertical-center" class="content-block">
                <h2>Vertical Center</h2>
                <?php //include INC_PATH . 'vertical-center.html' ?>
              </div>

              <div id="fullscreen-block" class="content-block">
                <h2>Fullscreen Block</h2>
                <?php //include INC_PATH . 'fullscreen-block.html' ?>
              </div>

            </div>

          </div>
        </div>
      </div>
      
    </section>

    <footer id="footer" class="section">
      <div class="content">
        <p>Footer to be defined ...</p>
      </div>
    </footer>


    <?php include(SRC_PATH.'templates.tpl'); ?>

  </body>

</html>
