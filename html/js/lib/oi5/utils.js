/*! utils.js v1.2.1 | MIT License | (c) 2015 by Felix Laukel (http://laukel.com) */

window.oi5 = window.oi5 || {};

(function (public, $, undefined) {


  // console polyfill
  if (!window.console) {
    window.console = {
      log: function(){},
      warn: function(){},
      error: function(){}
    };
  }

  // requestAnimationFrame polyfill
  if (!window.requestAnimationFrame) {

    window.requestAnimationFrame = (function() {

      return window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function(callback, element) {

        window.setTimeout(callback, 1000 / 60);

      };

    })();
  }

  // Object.create polyfill
  if (!Object.create) {
    Object.create = (function() {
      function F(){}

      return function(o) {
        if (arguments.length !== 1) {
          throw new Error('Object.create implementation only accepts one parameter.');
        }
        F.prototype = o;
        return new F();
      }
    })();
  }

  // String.trim polyfill
  if (!String.prototype.trim) {  
    String.prototype.trim = function() {  
      return this.replace(/^\s+|\s+$/gm,'');
    };  
  }


  // helper functions
  public.linkMail = function(name, domain) {
    document.location.href = 'mailto:' + name + '@' + domain;
  }

  public.reverseString = function(str) {
    return str.split('').reverse().join('');
  }

  public.getURLParameter = function(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
  }

  public.testCSSClass = function(cssClass, expr) {
    var id = 'test-css-class';
    var $elem = ($('#'+id).length === 0) ? $('<div id="'+id+'" />').appendTo('body') : $('#'+id);
    var expr = expr || 'visible';
    var result = $elem.removeClass().addClass(cssClass).is(':'+expr);
    $elem.removeClass();
    return result;
  }

  public.scrollTo = function (selector, time, offset) {
    var offset = offset || 0;
    var $target = $(selector).filter(':first');
    if (!$target.length) return;

    // cache target scroll position
    var max = $(document).height() - $(window).height();
    var position = $target.offset().top + offset;
    position = Math.min(max, position);
    // calc distance to scroll
    var dist = Math.abs($(window).scrollTop() - position) / $(window).height();
    dist = Math.max(1, dist);

    $('html, body').stop(true).animate({
      scrollTop: position
    }, (time || 500) * dist);
  }

  // plugin version of scrollTo for convenience
  if (!$.fn.scrollTo) {
    $.fn.scrollTo = function(time, offset) {
      public.scrollTo(this, time, offset);
      return this;
    }
  }


  // duplicate dom elements by comment *EXPERIMENTAL*
  var commentsEnabled = false;
  public.useComments = function() {
    var clipboard = {};
    commentsEnabled = true;

    $.fn.parseComments = function(options) {

      return this.each(function() {
          
        $(this).find('*').parent().contents().filter(function() {

          return this.nodeType === 8; //Node.COMMENT_NODE
          
        }).each(function() {

          var text = this.nodeValue.trim();
          var values = text.split(':');
          var command = values[0];
          var param = values[1];
          var $target = $(this).next();

          switch(command) {
            case 'REPEAT':
              var n = parseInt(param);
              for (var i = 1; i < n; i++) $target.after( $target.clone().parseComments() );
              break;

            case 'COPY':
              var id = param.trim();
              clipboard[id] = $target.get(0);
              break;

            case 'PASTE':
              var id = param.trim();
              $(this).after( $(clipboard[id]).clone().parseComments() );
              break;
          }

        });

      });

    }
    
  }
  
  $(document).ready(function() {
    // hook to execute parsing of comments before other widgets
    if (commentsEnabled) $('body').parseComments();

    // fix problem with testing css classes if using client-side less compiler
    if (typeof less !== 'undefined') {
      less.pageLoadFinished.then(
        function() {
          setTimeout(function() {
            $(window).trigger('resize').trigger('scroll');
          }, 100);
        }
      );
    }
  });




  // jquery plugin for simple 'animationend' callbacks
  $.fn.playAnimation = function(cssClass, callback) {
    var $self = this;

    if (!Modernizr || (Modernizr && Modernizr.cssanimations)) {

      $self.addClass(cssClass).one('animationend webkitAnimationEnd MSAnimationEnd', function() {
        $self.removeClass(cssClass);
        if (callback instanceof Function) callback.apply(this);
      });
      
    } else { // fallback for browser lack of css animation support

      $self.each(function() {
        if (callback instanceof Function) callback.apply(this);
      });
      
    }
    return this;
  }





  public.getTemplate = function(name) { return $('script[data-template='+name+']:first').html() };



})( window.oi5.utils = {}, jQuery );