<script data-template="dialog" type="text/x-template">
  <div class="dialog">
    <form class="dialog-frame">
      <header hidden class="dialog-title">Dialog Title</header>
      <div class="dialog-body">Dialog Body</div>
      <div class="dialog-input">
        <button class="dialog-confirm btn icon-checkmark">OK</button>
        <button class="dialog-cancel btn icon-close">Cancel</button>
      </div>
    </form>
  </div>
</script>