/*! objects.js v1.0.1 | MIT License | (c) 2015 by Felix Laukel (http://laukel.com) */

window.oi5 = window.oi5 || {};

(function (public, $, undefined) {


  // default settings
  public.defaults = {
    dialog: {
      template: 'dialog',
      parent: 'body',
      title: null,
      message: 'Are you sure?',
      body: null
    },
    ajax: {
      dataType: 'json',
      type: 'POST',
      data: {}
    }
  } 








  /* CLASS PATTERN

  public.MyClass = (function(superClass) {
    var exports = function(param) {

      superClass.call(this);

      this.attribute = param || false;

    };

    exports.prototype = Object.create(superClass.prototype);

    exports.prototype.init = function() {
      return this;
    }

    return exports;
  })(Object);

  */



  public.Trigger = (function(superClass) {
    var exports = function() {

      superClass.call(this);

      this.listeners = {};

    };

    exports.prototype = Object.create(superClass.prototype);

    exports.prototype.on = function(event, func) {
      if (this.listeners[event] === undefined) {
        this.listeners[event] = [];
      }

      if (this.listeners[event].indexOf(func) === -1) {
        this.listeners[event].push(func);
      }

      return this;
    }

    exports.prototype.off = function(event, func) {
      if (event instanceof String) {
        if (func instanceof Function) {
          this.listeners[event].splice( this.listeners[event].indexOf(func), 1 );
        } else {
          this.listeners[event] = [];
        }
      }

      return this;
    }

    exports.prototype.trigger = function(event, args) {
      var listeners = this.listeners[event];

      if (listeners instanceof Array) {
        for (var i = 0; i < listeners.length; i++) {
          if (listeners[i] instanceof Function) listeners[i].apply(this, args);
        }
      }

      return this;
    }

    return exports;
  })(Object);










  public.DeferredObject = (function(superClass) {
    var exports = function() {

      superClass.call(this);

      this.deferredDoneQueue = [];
      this.deferredFailQueue = [];
      this.deferredQueue = [];

    };

    exports.prototype = Object.create( superClass.prototype );

    exports.prototype.init = function() {
      return this;
    }

    exports.prototype.proceed = function(status, args) {

      var doneCallback = this.deferredDoneQueue.shift();
      var failCallback = this.deferredFailQueue.shift();

      if (status === false) {
        if (failCallback) { failCallback.apply(this, args); return this; }
      } else if (status === true) {
        if (doneCallback) { doneCallback.apply(this, args); return this; }
      }

      var callback = this.deferredQueue.shift();
      if (callback) { callback.apply(this, args); }

      return this;

    }
    exports.prototype.done = function(func) {
      if (func instanceof Function) this.deferredDoneQueue.push(func);
      return this;
    }

    exports.prototype.fail = function(func) {
      if (func instanceof Function) this.deferredFailQueue.push(func);
      return this;
    }

    exports.prototype.then = function(func) {
      if (func instanceof Function) this.deferredQueue.push(func);
      return this;
    }

    return exports;

  })(public.Trigger);









  // abstract ajax request class: use as template
  public.ARequest = (function(superClass) {
    var exports = function(options) {

      superClass.call(this);

      var self = this;

      var settings = $.extend(true, public.defaults.ajax, options);

      if (!settings.url) {
        throw new Error('no request url set');
      }

      $.ajax(
        settings
      ).done(function(data) {
        self.trigger('done', [data]).proceed(true, [data]);
      }).fail(function(data) {
        self.trigger('fail', [data]).proceed(false, [data]);
      });

    };

    exports.prototype = Object.create(superClass.prototype);

    exports.prototype.init = function() {
      return this;
    }

    return exports;

  })(public.DeferredObject);









  public.Dialog = (function(superClass) {
    var exports = function(options) {

      superClass.call(this);

      this.config = {};
      this.$element = null;

      this.alert(options); // defaults to alert

    };

    exports.prototype = Object.create(superClass.prototype);

    exports.prototype.init = function(options) {

      if (typeof options === 'string') {
        var o = { message: options }; options = o;
      }

      this.config = $.extend({}, public.defaults.dialog, options, this.config);

      if (this.$element) this.$element.remove();

      this.$element = $(oi5.utils.getTemplate(this.config.template)).appendTo(this.config.parent);

      if (this.config.title) this.$element.find('.dialog-title').text(this.config.title);
      if (!this.config.body) this.$element.find('.dialog-body').html(this.config.message);
      else this.$element.find('.dialog-body').empty().append(this.config.body);

      this.initEvents();

      return this;
    }

    exports.prototype.alert = function(options) {

      this.init(options);

      this.$element.find('.dialog-confirm').show();
      this.$element.find('.dialog-cancel').hide();

      return this;
    }

    exports.prototype.confirm = function(options) {

      this.init(options);

      this.$element.off('click');

      this.$element.find('.dialog-confirm').show();
      this.$element.find('.dialog-cancel').show();

      return this;
    }

    exports.prototype.initEvents = function() {

      var self = this;

      var $input = this.$element.find('.dialog-input');

      $input.find('.dialog-confirm').on('click', function(e) {
        self.trigger('ok').trigger('confirm').proceed(true);
      });

      $input.find('.dialog-cancel').on('click', function(e) {
        self.trigger('cancel').proceed(false);
      });

      self.$element.find('form').on('submit', function(e) {
        e.preventDefault();
        self.close();
      });

      self.$element.on('click', function(e) {
        if(!$(e.target).closest('.dialog-frame').length) {
          self.close();
        }
      });

      return this;
    }

    exports.prototype.close = function() {

      this.trigger('close').proceed();
      this.$element.remove();

      return this;
    }

    return exports;
  })(public.DeferredObject);









  public.Timer = (function(superClass) {
    var t = null, // current time counter
        p = null; // last pause time

    var exports = function(prec) {

      superClass.call(this);

      this.precision = prec || 0;
      this.running = false;
      this.paused = false;

    };

    exports.prototype = Object.create(superClass.prototype);

    exports.prototype.init = function() {
      return this;
    }

    exports.prototype.start = function() {
      (p !== null) ? this.resume() : t = new Date().getTime();
      this.running = true;
    }
    
    exports.prototype.elapsed = function(prec) {
      if (t !== null) {
        return Number(((p || new Date().getTime()) - t)*0.001).toFixed(prec || this.precision);
      } else {
        return Number(0).toFixed(this.precision); 
      }
    }
    exports.prototype.pause = function() {
      p = new Date().getTime();
      this.paused = true;
      return this.elapsed();
    }
    exports.prototype.resume = function() {
      t += new Date().getTime() - p;
      p = null;
      this.paused = false;
    }
    exports.prototype.stop = function() {
      var val = this.elapsed();
      t = p = null;
      this.running = false; 
      return val;
    }

    return exports;

  })(Object);






})( window.oi5.objects = {}, jQuery );