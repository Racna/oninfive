<?php

  require "lessc.inc.php";

  if (!defined('OUTPUT_PATH')) { define('OUTPUT_PATH', './'); }

  $less = new lessc;
  $less->setFormatter('compressed');
  $less->compileFile(OUTPUT_PATH.'less/style.less', OUTPUT_PATH.'css/style.css');

  function autoCompileLess($less, $inputFile, $outputFile) {
    $cacheFile = $inputFile.".cache";
    $cache = (file_exists($cacheFile)) ? unserialize(file_get_contents($cacheFile)) : $inputFile;
    $newCache = $less->cachedCompile($cache);
    if (!is_array($cache) || $newCache['updated'] > $cache['updated']) {
      file_put_contents($cacheFile, serialize($newCache));
      file_put_contents($outputFile, $newCache['compiled']);
    }
  }

  //autoCompileLess($less, OUTPUT_PATH.'less/style.less', OUTPUT_PATH.'css/style.css');

?>